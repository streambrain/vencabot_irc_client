# irc_client_vencabot
## Description
This module provides a simple Python 'IRCClient' object which can be used to send messages to and read messages from an IRC server.

## Status
As of right now, this module is fully-working although it only provides a very simplistic interface to IRC. It could probably use some better error-handling, at the very least.

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
